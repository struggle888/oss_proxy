package auth

import (
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/farmerx/gorsa"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"time"
)

const Version uint8 = 0

type Type uint8

const AccessToken Type = 0
const RefreshToken Type = 1

type Token struct {
	Version    uint8
	ExpireTime uint32
	Type       Type

	// content
	UserId string
}

func (token Token) Bytes() []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, token.ExpireTime)
	return append(append([]byte{token.Version}, append(b, byte(token.Type))...), []byte(token.UserId)...)
}

func parseToken(b []byte) (Token, error) {
	token := Token{}
	if len(b) < 7 {
		return token, fmt.Errorf("too short token, get %d", len(b))
	}
	token.Version = b[0]
	switch b[0] {
	case 0:
		token.ExpireTime = binary.BigEndian.Uint32(b[1:5])
		token.Type = Type(b[5])
		token.UserId = string(b[6:])
	default:
		return token, fmt.Errorf("unknown versiion: %d", b[0])
	}
	return token, nil
}

func InitPubKey(publicKeyFile string) error {
	b, err := ioutil.ReadFile(publicKeyFile)
	if err != nil {
		return err
	}
	err = gorsa.RSA.SetPublicKey(string(b))
	return err
}

func ParseToken(s string) (Token, error) {
	t := Token{}
	b, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil {
		return t, err
	}
	decrypted, err := gorsa.RSA.PubKeyDECRYPT(b)
	if err != nil {
		return t, err
	}
	return parseToken(decrypted)
}

func CheckToken(c *gin.Context) (string, error) {
	tokenString := c.DefaultQuery("authentication", c.DefaultQuery("token", c.DefaultPostForm("authentication", c.DefaultPostForm("token", c.GetHeader("authentication")))))
	if tokenString == "" {
		return "", errors.New("unauthorized")
	}
	token, err := ParseToken(tokenString)
	if err != nil || token.Type != AccessToken {
		return "", errors.New("error in checking token")
	}
	if token.ExpireTime < uint32(time.Now().Unix()) {
		return "", errors.New("expired token")
	}
	return token.UserId, nil
}

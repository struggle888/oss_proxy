package main

import (
	"bitbucket.org/struggle888/oss_proxy/external/auth"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"hash"
	"io"
	"io/ioutil"
	"net/url"
	"strings"
	"time"
)

var logger = initLogger(false)
var configFilePath = "config.json"
var publicKeyPath = "public.pem"
var config gjson.Result
var AccessId string
var AccessKey string

func getOssFile(c *gin.Context) {
	_, err := auth.CheckToken(c)
	if err != nil {
		c.JSON(403, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}
	u := c.Query("url")
	if u == "" {
		c.JSON(400, gin.H{
			"status":  false,
			"message": "invalid params",
		})
		return
	}
	uu, err := url.Parse(u)
	if err != nil {
		c.JSON(400, gin.H{
			"status":  false,
			"message": "invalid url",
		})
		return
	}
	bucket := strings.Split(uu.Host, ".")[0]
	expire := time.Now().Unix() + 60
	h := hmac.New(func() hash.Hash { return sha1.New() }, []byte(AccessKey))
	_, _ = io.WriteString(h, fmt.Sprintf("GET\n\n\n%d\n/%s%s", expire, bucket, uu.Path))
	c.Redirect(302, fmt.Sprintf("%s?OSSAccessKeyId=%s&Expires=%d&Signature=%s", u, AccessId, expire, url.QueryEscape(base64.StdEncoding.EncodeToString(h.Sum(nil)))))
}

func RunApi() {
	b, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		logger.Fatalf("error in reading config file: %v", err)
	}
	config = gjson.ParseBytes(b)
	AccessId = config.Get("access_id").String()
	AccessKey = config.Get("access_key").String()
	if AccessKey == "" || AccessId == "" {
		logger.Fatalf("unspecified access key or id")
	}
	if config.Get("public_key").Exists() {
		publicKeyPath = config.Get("public_key").String()
	}
	err = auth.InitPubKey(publicKeyPath)
	if err != nil {
		logger.Fatalf("error in init public key: %v", err)
	}

	router := gin.Default()
	router.Use(recovery())

	router.GET("/", getOssFile)

	_ = router.Run(":24400")
}

func main() {
	RunApi()
}

func initLogger(debug bool) *zap.SugaredLogger {
	cfg := zap.NewProductionConfig()
	cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	if debug {
		cfg.Level.SetLevel(zapcore.DebugLevel)
	} else {
		cfg.Level.SetLevel(zapcore.InfoLevel)
	}
	logger, _ := cfg.Build()
	defer logger.Sync() // flushes buffer, if any
	return logger.Sugar()
}

func recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
				c.String(500, "server internal error")
			}
		}()
		c.Next()
	}
}

module bitbucket.org/struggle888/oss_proxy

go 1.14

require (
	github.com/farmerx/gorsa v0.0.0-20161211100049-3ae06f674f40
	github.com/gin-gonic/gin v1.7.7
	github.com/tidwall/gjson v1.14.0
	go.uber.org/zap v1.21.0
)
